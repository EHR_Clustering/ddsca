The folder contains the source code of the paper:

Clustering demographics and sequences of diagnosis codes by Haodi Zhong, Grigorios Loukides and Solon P. Pissis (IEEE Journal of Biomedical and Health Informatics).

To compile the source code please follow the instructions in INSTALL.txt.

For convenience, we have included one dataset (INFORMS) we used. The dataset is publicly available at:

https://sites.google.com/site/informsdataminingcontest/data/

For MIMIC, please request access from 'https://mimic.physionet.org/gettingstarted/access/'
